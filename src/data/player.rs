use rand::{seq::SliceRandom, thread_rng, Rng};
use strum::{
    AsRefStr, AsStaticStr, EnumCount, EnumIter, EnumString, IntoEnumIterator, IntoStaticStr,
};

use super::{ArmourSlot, Equipment, Inventory, WeaponSlot, MAX_ITEMS};

#[derive(Debug)]
pub struct Player {
    // Name of the player
    pub name: String,

    // Player stats
    pub str: u8,
    pub dex: u8,
    pub int: u8,

    // Player's race & class
    pub race: Race,
    pub class: Class,

    // Player's inventory
    pub inventory: Inventory,
    pub equipment: Equipment,

    // Player's experience
    pub xp: u64,
}

// The possible races the player can be
#[derive(EnumIter, IntoStaticStr, Clone, Copy, Debug)]
pub enum Race {
    Human,
    Elf,
    Dwarf,
    Orc,
    Goblin,
    Troll,
    Ogre,
    Halfling,
}

// The possible classes the player can be
#[derive(EnumIter, IntoStaticStr, Clone, Copy, Debug)]
pub enum Class {
    Warrior,
    Mage,
    Rogue,
}

impl Player {
    pub fn random() -> Self {
        let mut r = thread_rng();

        let race = *Race::iter()
            .collect::<Vec<_>>()
            .choose(&mut r)
            .unwrap_or(&Race::Human);
        let class = *Class::iter()
            .collect::<Vec<_>>()
            .choose(&mut r)
            .unwrap_or(&Class::Warrior);

        let name = String::from("Mr. Nice");

        let (str, dex, int) = match class {
            Class::Warrior => (r.gen_range(12..19), r.gen_range(10..16), r.gen_range(5..12)),
            Class::Rogue => (r.gen_range(10..16), r.gen_range(12..19), r.gen_range(5..12)),
            Class::Mage => (r.gen_range(5..12), r.gen_range(10..16), r.gen_range(12..19)),
        };

        let (str, dex, int) = match race {
            Race::Human => (str, dex, int),
            Race::Elf => (str - 1, dex + 1, int),
            Race::Dwarf => (str + 1, dex - 1, int),
            Race::Orc => (str, dex, int),
            Race::Goblin => (str - 2, dex, int + 2),
            Race::Troll => (str + 2, dex + 1, int - 3),
            Race::Ogre => (str + 2, dex, int - 2),
            Race::Halfling => (str - 2, dex + 1, int + 1),
        };

        let inventory = Inventory::new();
        let equipment = Equipment::new();

        Self {
            name,
            str,
            dex,
            int,
            race,
            class,
            inventory,
            equipment,
            xp: 0,
        }
    }

    pub fn is_bags_full(&self) -> bool {
        self.inventory.is_full()
    }

    pub fn level(&self) -> u8 {
        if self.xp < 100 {
            1
        } else if self.xp < 250 {
            2
        } else if self.xp < 450 {
            3
        } else if self.xp < 700 {
            4
        } else {
            5
        }
    }
}
