use strum::EnumCount;

use super::{Armour, ArmourSlot, InventorySlot, Weapon, WeaponSlot};

#[derive(Debug)]
pub struct Equipment {
    pub armour: Vec<Option<Armour>>,
    pub weapons: Vec<Option<Weapon>>,
}

impl Equipment {
    pub fn new() -> Self {
        Self {
            armour: vec![None; ArmourSlot::COUNT],
            weapons: vec![None; WeaponSlot::COUNT],
        }
    }
}
