#[derive(Debug)]
pub struct Money {
    pub platinum: u32,
    pub gold: u8,
    pub silver: u8,
    pub copper: u8,
}
