use super::{Armour, ArmourSlot, Item, Monster, Weapon, WeaponSlot, WeaponType};

#[derive(Debug)]
pub struct World {
    pub locations: Vec<Location>,
}

#[derive(Debug)]
pub struct Location {
    pub name: String,
    pub description: String,
    pub level: u8,
    pub monsters: Vec<Monster>,
}

impl World {
    pub fn new() -> Self {
        let locations = vec![
            Location {
                name: "the Forest".to_string(),
                description: "A dark and scary forest".to_string(),
                level: 1,
                monsters: vec![
                    Monster::new(
                        "Goblin",
                        1,
                        vec![
                            Item::new_weapon(
                                "Rusty Sword",
                                1,
                                WeaponSlot::MainHand,
                                WeaponType::Sword,
                            ),
                            Item::new_armour("Rusty Armour", 1, ArmourSlot::Chest),
                            Item::new_trash("Rusty Coin", 1),
                            Item::new_trash("Ripped Cloth", 1),
                            Item::new_trash("Broken Dagger", 1),
                        ],
                    ),
                    Monster::new(
                        "Wolf",
                        1,
                        vec![
                            Item::new_weapon(
                                "Wolf Fang",
                                1,
                                WeaponSlot::MainHand,
                                WeaponType::Dagger,
                            ),
                            Item::new_trash("Wolf Pelt", 1),
                        ],
                    ),
                    Monster::new(
                        "Bear",
                        2,
                        vec![
                            Item::new_weapon(
                                "Bear Claw",
                                2,
                                WeaponSlot::MainHand,
                                WeaponType::Dagger,
                            ),
                            Item::new_trash("Bear Pelt", 2),
                        ],
                    ),
                    Monster::new(
                        "Dire Wolf",
                        2,
                        vec![
                            Item::new_weapon(
                                "Dire Wolf Fang",
                                2,
                                WeaponSlot::MainHand,
                                WeaponType::Dagger,
                            ),
                            Item::new_trash("Dire Wolf Pelt", 2),
                        ],
                    ),
                ],
            },
            Location {
                name: "the Plains".to_string(),
                description: "A wide open plain".to_string(),
                level: 1,
                monsters: vec![
                    Monster::new(
                        "Orc",
                        2,
                        vec![
                            Item::new_weapon(
                                "Orc Sword",
                                2,
                                WeaponSlot::MainHand,
                                WeaponType::Sword,
                            ),
                            Item::new_trash("Orc Coin", 2),
                        ],
                    ),
                    Monster::new(
                        "Goblin",
                        1,
                        vec![
                            Item::new_weapon(
                                "Rusty Sword",
                                1,
                                WeaponSlot::MainHand,
                                WeaponType::Sword,
                            ),
                            Item::new_armour("Rusty Armour", 1, ArmourSlot::Chest),
                            Item::new_trash("Rusty Coin", 1),
                            Item::new_trash("Ripped Cloth", 1),
                            Item::new_trash("Broken Dagger", 1),
                        ],
                    ),
                ],
            },
            Location {
                name: "the Mountains".to_string(),
                description: "A tall mountain range".to_string(),
                level: 2,
                monsters: vec![Monster::new(
                    "Troll",
                    3,
                    vec![
                        Item::new_weapon("Troll Club", 3, WeaponSlot::MainHand, WeaponType::Mace),
                        Item::new_trash("Rock", 1),
                    ],
                )],
            },
        ];
        World { locations }
    }
}
