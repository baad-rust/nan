use rand::Rng;

use super::Item;

#[derive(Debug, Clone)]
pub struct Monster {
    pub name: String,
    pub level: u8,
    pub items: Vec<Item>,
}

impl Monster {
    pub fn new(name: &str, level: u8, items: Vec<Item>) -> Self {
        Self {
            name: name.to_string(),
            level,
            items,
        }
    }

    pub fn choose_item(&self) -> Option<Item> {
        let mut rng = rand::thread_rng();
        let index = rng.gen_range(0..self.items.len());
        self.items.get(index).cloned()
    }
}
