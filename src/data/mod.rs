pub mod armour;
pub mod equipment;
pub mod inventory;
pub mod location;
pub mod money;
pub mod monster;
pub mod player;
pub mod weapon;

pub use armour::*;
pub use equipment::*;
pub use inventory::*;
pub use location::*;
pub use money::*;
pub use monster::*;
pub use player::*;
pub use weapon::*;

pub const MAX_ITEMS: usize = 8;
pub const MAX_STACK_SIZE: u8 = 16;
