use strum::{EnumCount, EnumIter, IntoStaticStr};

#[derive(EnumCount, EnumIter, IntoStaticStr, Clone, Copy, Debug)]
pub enum ArmourSlot {
    Head,
    Chest,
    Legs,
    Feet,
    Hands,
}

#[derive(Debug, Clone)]
pub struct Armour {
    pub name: String,
    pub level: u8,
    pub slot: ArmourSlot,
}

impl Armour {
    pub fn new(name: &str, level: u8, slot: ArmourSlot) -> Self {
        Self {
            name: name.to_string(),
            level,
            slot,
        }
    }
}
