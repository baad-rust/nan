use super::{Armour, ArmourSlot, Weapon, WeaponSlot, WeaponType, MAX_ITEMS, MAX_STACK_SIZE};

#[derive(Debug, Clone)]
pub enum Item {
    Weapon(Weapon),
    Armour(Armour),
    Trash { name: String, level: u8, count: u8 },
}

#[derive(Debug, Clone)]
pub struct InventorySlot {
    pub item: Item,
}

#[derive(Debug)]
pub struct Inventory {
    pub slots: Vec<Option<InventorySlot>>,
}

impl Item {
    pub fn new_weapon(name: &str, level: u8, slot: WeaponSlot, weapon_type: WeaponType) -> Self {
        Self::Weapon(Weapon::new(name, level, slot, weapon_type))
    }

    pub fn new_armour(name: &str, level: u8, slot: ArmourSlot) -> Self {
        Self::Armour(Armour::new(name, level, slot))
    }

    pub fn new_trash(name: &str, level: u8) -> Self {
        Self::Trash {
            name: name.to_string(),
            level,
            count: 1,
        }
    }
}

impl InventorySlot {
    pub fn new(item: Item) -> Self {
        Self { item }
    }

    pub fn is_full(&self) -> bool {
        match &self.item {
            Item::Weapon(_) | Item::Armour(_) => true,
            Item::Trash { count, .. } => *count == MAX_STACK_SIZE,
        }
    }
}

impl Inventory {
    pub fn new() -> Self {
        Self {
            slots: vec![None; MAX_ITEMS],
        }
    }

    pub fn is_full(&self) -> bool {
        self.slots.iter().all(|slot| {
            if let Some(slot) = slot {
                slot.is_full()
            } else {
                false
            }
        })
    }

    pub fn add(&mut self, item: Item) -> bool {
        match item {
            Item::Weapon(w) => self.add_weapon(w),
            Item::Armour(a) => self.add_armour(a),
            Item::Trash { name, level, count } => self.add_trash(name, level, count),
        }
    }

    fn add_weapon(&mut self, weapon: Weapon) -> bool {
        self.add_item_to_empty_slot(Item::Weapon(weapon))
    }

    fn add_armour(&mut self, armour: Armour) -> bool {
        self.add_item_to_empty_slot(Item::Armour(armour))
    }

    fn add_item_to_empty_slot(&mut self, item: Item) -> bool {
        if let Some(slot) = self.slots.iter_mut().find(|slot| slot.is_none()) {
            *slot = Some(InventorySlot::new(item));
            true
        } else {
            false
        }
    }

    fn find_available_stack(&mut self, name: &String, level: u8) -> Option<&mut u8> {
        self.slots
            .iter_mut()
            // Convert &Option<InventorySlot> to Option<&mut u8> referencing the count
            .filter_map(|slot| {
                if let Some(slot) = slot {
                    match &mut slot.item {
                        Item::Trash {
                            name: slot_name,
                            level: slot_level,
                            count,
                        } if name == slot_name
                            && level == *slot_level
                            && *count < MAX_STACK_SIZE =>
                        {
                            Some(count)
                        }
                        _ => None,
                    }
                } else {
                    None
                }
            })
            .nth(0)
    }

    fn add_trash(&mut self, name: String, level: u8, mut count: u8) -> bool {
        // Try to add it to an existing stack
        while count > 0 {
            let slot = self.find_available_stack(&name, level);

            if let Some(count_slot) = slot {
                let max_count = MAX_STACK_SIZE - *count_slot;
                let count_to_add = u8::min(max_count, count);
                *count_slot += count_to_add;
                count -= count_to_add;
            } else {
                break;
            }
        }

        // Try to add it to an empty slot
        if count > 0 {
            let slot = self.slots.iter_mut().find(|slot| slot.is_none());
            if let Some(slot) = slot {
                *slot = Some(InventorySlot::new(Item::Trash { name, level, count }));
                true
            } else {
                false
            }
        } else {
            true
        }
    }
}
