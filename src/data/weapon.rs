use strum::{EnumCount, EnumIter, IntoStaticStr};

#[derive(EnumCount, EnumIter, IntoStaticStr, Clone, Copy, Debug)]
pub enum WeaponSlot {
    MainHand,
    OffHand,
    TwoHand,
    Ranged,
}

#[derive(Debug, Clone, Copy)]
pub enum WeaponType {
    Sword,
    Axe,
    Mace,
    Dagger,
    Bow,
    Crossbow,
    Staff,
    Wand,
}

#[derive(Debug, Clone)]
pub struct Weapon {
    pub name: String,
    pub level: u8,
    pub slot: WeaponSlot,
    pub weapon_type: WeaponType,
}

impl Weapon {
    pub fn new(name: &str, level: u8, slot: WeaponSlot, weapon_type: WeaponType) -> Self {
        Self {
            name: name.to_string(),
            level,
            slot,
            weapon_type,
        }
    }
}
