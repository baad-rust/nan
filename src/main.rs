#![allow(unused)]

mod adventure;
mod data;

use std::{error::Error, io::stdout, time::Duration};

use adventure::Adventure;
use crossterm::{
    event::{poll, read, DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyEvent},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use data::{Armour, ArmourSlot, Item, Weapon, WeaponSlot};
use ratatui::{
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Direction, Layout},
    style::{Color, Style, Styled},
    text::{Line, Span, Text},
    widgets::{Block, Borders, Clear, Paragraph, Row, Table},
    Terminal,
};
use strum::IntoEnumIterator;

use crate::data::{Player, World};

fn main() -> Result<(), Box<dyn Error>> {
    let mut out = stdout();
    let backend = CrosstermBackend::new(out);
    let mut terminal = Terminal::new(backend)?;

    enable_raw_mode()?;
    let mut out = stdout();
    execute!(out, EnterAlternateScreen, EnableMouseCapture)?;

    terminal.hide_cursor()?;

    let mut player = Player::random();
    let world = World::new();

    let mut adventure = Some(Adventure::new(&mut player, &world.locations[0]));

    loop {
        if let Ok(true) = poll(Duration::ZERO) {
            if let Event::Key(key) = read().unwrap() {
                match key.code {
                    KeyCode::Char('q') => break,
                    _ => {}
                }
            }
        }

        // Run adventure
        if let Some(adventure) = adventure.as_mut() {
            let player_status = if let Some((event, time_left)) = adventure.next() {
                format!("{} ({}s)", event, time_left.as_secs())
            } else {
                "Resting".to_string()
            };

            terminal.draw(|f| {
                // Set up the layout for the terminal output
                let chunks = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints(
                        [
                            Constraint::Length(3),
                            Constraint::Min(1),
                            Constraint::Length(3),
                        ]
                        .as_ref(),
                    )
                    .split(f.size());

                // Draw the title
                let title_block = Block::default().borders(Borders::ALL);
                let title = Paragraph::new("Nor'tee & Nice")
                    .block(title_block)
                    .alignment(Alignment::Center);
                f.render_widget(title, chunks[0]);

                // Draw the status
                let status_block = Block::default().borders(Borders::ALL);
                let status = Paragraph::new("").block(status_block);
                f.render_widget(status, chunks[2]);

                // Draw the main content
                let player_chunks = Layout::default()
                    .direction(Direction::Horizontal)
                    .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
                    .split(chunks[1]);

                // Draw the player info
                let player = &adventure.player;
                let player_race: &'static str = player.race.into();
                let player_class: &'static str = player.class.into();
                let player_table_widths = [
                    Constraint::Length(8),
                    Constraint::Min(player_chunks[0].width - 9),
                ];
                let col0_style = Style::default().fg(Color::Green);
                let col1_style = Style::default().fg(Color::White);

                let rows = [
                    ("Name", player.name.clone()),
                    ("Race", player_race.to_string()),
                    ("Class", player_class.to_string()),
                    ("Level", player.level().to_string()),
                    ("XP", player.xp.to_string()),
                ];

                let player_rows = rows
                    .iter()
                    .map(|(label, value)| {
                        Row::new(vec![
                            label.to_string().set_style(col0_style),
                            value.to_string().set_style(col1_style),
                        ])
                    })
                    .collect::<Vec<_>>();

                let player_details = Table::new(player_rows).widths(&player_table_widths);

                let info_chunks = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints(
                        [
                            Constraint::Length(rows.len() as u16),
                            Constraint::Length(1),
                            Constraint::Min(1),
                        ]
                        .as_ref(),
                    )
                    .split(player_chunks[0]);

                f.render_widget(player_details, info_chunks[0]);

                // Draw the player's status
                let player_status = Paragraph::new(vec![
                    Line::from(Span::styled(
                        "Location",
                        Style::default().fg(Color::Black).bg(Color::Yellow),
                    )),
                    Line::from(adventure.location.name.clone()),
                    Line::from(adventure.location.description.clone()),
                    Line::from(""),
                    Line::from(Span::styled(
                        "Status",
                        Style::default().fg(Color::Black).bg(Color::Yellow),
                    )),
                    Line::from(player_status),
                ]);
                f.render_widget(player_status, info_chunks[2]);

                // Draw the player equipment and inventory
                let inventory_block = Block::default().borders(Borders::ALL).title("Inventory");
                let inventory = Paragraph::new("").block(inventory_block);
                f.render_widget(inventory, player_chunks[1]);

                let armour_rows = ArmourSlot::iter()
                    .enumerate()
                    .map(|(index, slot)| {
                        let slot_name: &'static str = slot.into();
                        let slot_name = slot_name
                            .to_string()
                            .set_style(Style::default().fg(Color::Green));
                        let armour = player.equipment.armour.get(index).unwrap();
                        let armour = armour.clone().map_or_else(
                            || {
                                "Empty"
                                    .to_string()
                                    .set_style(Style::default().fg(Color::DarkGray))
                            },
                            |armour| {
                                armour
                                    .name
                                    .clone()
                                    .set_style(Style::default().fg(get_colour(armour.level)))
                            },
                        );
                        (slot_name, armour)
                    })
                    .map(|(slot_name, armour)| Row::new(vec![slot_name, armour]))
                    .collect::<Vec<_>>();
                let num_armour_rows = armour_rows.len() as u16;

                let armour_table_widths = [
                    Constraint::Length(12),
                    Constraint::Min(player_chunks[1].width - 14),
                ];
                let armour_table = Table::new(armour_rows)
                    .widths(&armour_table_widths)
                    .block(Block::default().borders(Borders::ALL).title("Armour"))
                    .header(
                        Row::new(vec!["Slot", "Armour"])
                            .set_style(Style::default().fg(Color::Magenta)),
                    );

                let equipment_slot_names = ["Main Hand", "Off Hand", "Two Hand", "Ranged"];
                let equipment_rows = WeaponSlot::iter()
                    .enumerate()
                    .map(|(index, slot)| {
                        let slot_name = equipment_slot_names[index]
                            .to_string()
                            .set_style(Style::default().fg(Color::Green));
                        let weapon = player.equipment.weapons.get(index).unwrap();
                        let weapon = weapon.clone().map_or_else(
                            || {
                                "Empty"
                                    .to_string()
                                    .set_style(Style::default().fg(Color::DarkGray))
                            },
                            |weapon| {
                                weapon
                                    .name
                                    .clone()
                                    .set_style(Style::default().fg(get_colour(weapon.level)))
                            },
                        );
                        (slot_name, weapon)
                    })
                    .map(|(slot_name, weapon)| Row::new(vec![slot_name, weapon]))
                    .collect::<Vec<_>>();
                let num_equipment_rows = equipment_rows.len() as u16;

                let equipment_table_widths = [
                    Constraint::Length(12),
                    Constraint::Min(player_chunks[1].width - 14),
                ];
                let equipment_table = Table::new(equipment_rows)
                    .widths(&equipment_table_widths)
                    .block(Block::default().borders(Borders::ALL).title("Equipment"))
                    .header(
                        Row::new(vec!["Slot", "Weapon"])
                            .set_style(Style::default().fg(Color::Magenta)),
                    );

                // Draw the player inventory
                let inventory_rows = player
                    .inventory
                    .slots
                    .iter()
                    .map(|slot| {
                        if let Some(slot) = slot {
                            match &slot.item {
                                Item::Weapon(Weapon { name, level, .. })
                                | Item::Armour(Armour { name, level, .. }) => {
                                    format!("   {}", name)
                                        .set_style(Style::default().fg(get_colour(*level)))
                                }

                                Item::Trash { name, count, .. } => format!("{count:2} {name}")
                                    .set_style(Style::default().fg(Color::DarkGray)),
                            }
                        } else {
                            Span::from("   Empty").set_style(Style::default().fg(Color::DarkGray))
                        }
                    })
                    .map(Line::from)
                    .collect::<Vec<_>>();

                let inventory_paragraph = Paragraph::new(inventory_rows)
                    .block(Block::default().borders(Borders::ALL).title("Inventory"))
                    .alignment(Alignment::Left);

                let inventory_chunks = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints(
                        [
                            Constraint::Length(num_armour_rows + 2),
                            Constraint::Length(num_equipment_rows + 2),
                            Constraint::Min(1),
                        ]
                        .as_ref(),
                    )
                    .margin(1)
                    .split(player_chunks[1]);

                f.render_widget(armour_table, inventory_chunks[0]);
                f.render_widget(equipment_table, inventory_chunks[1]);
                f.render_widget(inventory_paragraph, inventory_chunks[2]);

                // let p = Paragraph::new(format!("Status: {player_status}"));
                // f.render_widget(p, chunks[1]);
            });
        }
    }

    terminal.show_cursor()?;
    terminal.flush()?;

    execute!(out, DisableMouseCapture, LeaveAlternateScreen)?;
    disable_raw_mode()?;

    Ok(())
}

fn get_colour(level: u8) -> Color {
    match level {
        1 => Color::DarkGray,
        2 => Color::Gray,
        3 => Color::Green,
        4 => Color::Blue,
        _ => Color::Yellow,
    }
}
