use std::time::{Duration, Instant};

use rand::{rngs::ThreadRng, seq::SliceRandom};

use crate::data::{Item, Location, Monster, Player, World};

const BASE_TIME: u64 = 5;

#[derive(Debug, Eq, PartialEq)]
pub enum PlayerState {
    Travelling,
    Returning,
    Adventuring,
    Fighting,
    Done,
}

pub struct Adventure<'a> {
    pub player: &'a mut Player,
    pub location: &'a Location,
    rng: ThreadRng,

    current_action: (PlayerState, Instant),
    current_desc: String,
    next_monster: Option<&'a Monster>,
}

impl<'a> Adventure<'a> {
    pub fn new(player: &'a mut Player, location: &'a Location) -> Self {
        let player_name = player.name.clone();

        Self {
            player,
            location,
            rng: ThreadRng::default(),
            current_action: (
                PlayerState::Adventuring,
                Instant::now() + Duration::from_secs(BASE_TIME),
            ),
            current_desc: format!("{} is travelling to {}...", player_name, location.name),
            next_monster: None,
        }
    }
}

impl<'a> Iterator for Adventure<'a> {
    type Item = (String, Duration);

    fn next(&mut self) -> Option<Self::Item> {
        let now = Instant::now();

        if now >= self.current_action.1 {
            match self.current_action.0 {
                // The player is first setting out on their adventure
                PlayerState::Travelling => {}

                // The player is adventuring in the location and looking for the
                // next encounter.  We also decide the next monster here.
                PlayerState::Adventuring => {
                    let mut continue_fighting = true;

                    if let Some(monster) = self.next_monster {
                        // We've killed a monster previously, so we add the XP and loot
                        self.player.xp += monster.level as u64;

                        let loot = monster.items.choose(&mut self.rng);
                        if loot.is_some() {
                            if !self.player.inventory.add(loot.unwrap().clone()) {
                                self.current_action = (PlayerState::Returning, now);
                                self.current_desc = format!(
                                    "The bags are full so {} is returning home...",
                                    self.player.name
                                );
                                continue_fighting = false;
                            }
                        }
                    }

                    if continue_fighting {
                        self.current_action =
                            (PlayerState::Fighting, now + Duration::from_secs(BASE_TIME));
                        self.current_desc =
                            format!("{} is searching for an encounter...", self.player.name);
                        self.next_monster = self.location.monsters.choose(&mut self.rng);
                    }
                }

                PlayerState::Fighting => {
                    // Check to see if the player's inventory is full or we
                    // don't have a monster to fight.  If so, we return home.
                    // Otherwise, we continue fighting.
                    if self.next_monster.is_none() || self.player.is_bags_full() {
                        self.current_action = (PlayerState::Returning, now);
                        self.current_desc = format!(
                            "The bags are full so {} is returning home...",
                            self.player.name
                        );
                    } else {
                        let level_difference = self
                            .next_monster
                            .unwrap()
                            .level
                            .saturating_sub(self.player.level());
                        self.current_action = (
                            PlayerState::Adventuring,
                            now + Duration::from_secs(
                                BASE_TIME + BASE_TIME * level_difference as u64,
                            ),
                        );
                        self.current_desc = format!(
                            "{} is fighting a {}...",
                            self.player.name,
                            self.next_monster.unwrap().name
                        );
                    }
                }
                PlayerState::Returning => {
                    self.current_action = (PlayerState::Done, now + Duration::from_secs(BASE_TIME));
                }
                PlayerState::Done => {}
            }
        }

        if self.current_action.0 == PlayerState::Done {
            None
        } else {
            Some((self.current_desc.clone(), self.current_action.1 - now))
        }
    }
}
